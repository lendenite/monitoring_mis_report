package com.lendenclub.selenium;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class WebCapability {
	
	WebDriver driver;

	public WebDriver WebCapability() {
		
		try 
		{
			WebDriverManager.chromedriver().setup();
	        ChromeOptions opt = new ChromeOptions();
	        opt.addArguments("--headless=new");
	        opt.addArguments("window-size=1920,1180");
	        opt.addArguments("--disable-notification");
	        opt.addArguments("--remote-allow-origins=*");
	        driver = new ChromeDriver(opt);
		}
		catch(Exception e)
		{}
		
		File source = new File("./Resources");
		System.out.println(source);
		try {
			System.out.println("heloo");
			FileUtils.cleanDirectory(source);
		
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		return driver;
		
	}

}
