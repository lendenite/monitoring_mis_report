package com.lendenclub.test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.lendenclub.selenium.WebCapability;
import com.lendenclub.steps.Login;
import com.lendenclub.steps.ValidatingData;
import com.opencsv.CSVWriter;

public class TestExecution extends WebCapability{
	
	WebDriver driver;
	CSVWriter writer;
	
	@BeforeTest
	public void openingChromeBrowser()
	{	   	
		driver = WebCapability();
	}
	
	@Test(priority = 1)
	public void  loginPage()
	{
	  	new Login(driver);	
	}
	
	@Test(priority = 2)
	public void  CheckingData() throws Exception
	{
	  	new ValidatingData(driver,writer); 	
	}
	
	@AfterTest
	public void ClosingBrowser()
	{
		driver.quit();
	}

}
	