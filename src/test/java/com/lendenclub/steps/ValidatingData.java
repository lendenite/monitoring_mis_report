package com.lendenclub.steps;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.concurrent.TimeUnit;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.opencsv.CSVWriter;

public class ValidatingData {

	WebDriver driver;
	CSVWriter writer; //sandeep
	//	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjIyMSwiYnJlYWtvdXQiOltbImZpZWxkLWlkIiwyNjc4XSxbImZpZWxkLWlkIiwyNjY3XSxbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwyNjYzXSwibWludXRlIl0sWyJmay0-IixbImZpZWxkLWlkIiwyNjU5XSxbImZpZWxkLWlkIiw3NDhdXV0sImZpbHRlciI6WyJhbmQiLFsiPSIsWyJmaWVsZC1pZCIsMjY2N10sIjE3MjY2MjIxMTEiXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	String URL = "https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjIyMSwiYnJlYWtvdXQiOltbImZpZWxkLWlkIiwyNjc4XSxbImZpZWxkLWlkIiwyNjY3XSxbImRhdGV0aW1lLWZpZWxkIixbImZpZWxkLWlkIiwyNjYzXSwibWludXRlIl0sWyJmay0-IixbImZpZWxkLWlkIiwyNjU5XSxbImZpZWxkLWlkIiw3NDhdXSxbImpvaW5lZC1maWVsZCIsIkxvYW4iLFsiZmllbGQtaWQiLDEyOTldXV0sImZpbHRlciI6WyJhbmQiLFsiPSIsWyJmaWVsZC1pZCIsMjY2N10sIjE2ODQ4ODA1MyJdXSwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTExLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsMjY2NF0sWyJqb2luZWQtZmllbGQiLCJMb2FuIixbImZpZWxkLWlkIiwxMzEyXV1dLCJhbGlhcyI6IkxvYW4ifV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
	boolean test=false;
	String[] result1;
	String[] result2;
	String header[]= {"required_loan_id","amount","reference_id", "source", "Email_Id", "payment_dtm", "Automation_Status"};
	String values[] = {"", "", "", "", "", "", ""};

	@SuppressWarnings("deprecation")
	public ValidatingData(WebDriver driver, CSVWriter writer) throws Exception {

		Thread.sleep(4000);
		driver.navigate().to(URL);
		File filePath = null;
		try {

			File folder = new File("./fileData/");
			File[] files = folder.listFiles();
			for (File file : files) {
				if (file.isFile()) {
					filePath=file;

					// Get the file name - sandeep
					String fileName = filePath.getName();

					// Print the file name - sandeep
					System.out.println("File name: " + fileName);
					result1 = fileName.split(".xlsx");
					result2 =result1[0].split("Limited_");
					System.out.println("@@@@@@@@@@@@@@ " + result2[1]);
				}
			}
		}catch(Exception e)
		{}
		writer=new CSVWriter(new FileWriter("./Resources/"+result2[1]+"im.csv")); //sandeep

		FileInputStream fs = new FileInputStream(filePath);
		XSSFWorkbook workbook = new XSSFWorkbook(fs);
		XSSFSheet sheet = workbook.getSheetAt(0);
		DataFormatter formatter = new DataFormatter();
		int total_Rows = sheet.getLastRowNum();

		writer.writeNext(header); //sandeep

		sheet.getRow(16).createCell(sheet.getRow(16).getLastCellNum()).setCellValue("Automation Status[Correct]");
		sheet.getRow(16).createCell(((sheet.getRow(16).getLastCellNum()))).setCellValue("Automation Status[Wrong]");

		for( int i= 17; i <= total_Rows; i++)
		{	
			Row row = sheet.getRow(i);
			if(row == null)
			{
				continue;
			}

			Cell tpsl_cell = row.getCell(3);    //this is for reference id []
			Cell net_amount_cell = row.getCell(9);  // this is for net amount
			Cell email_cell = row.getCell(13);
			Cell trxn_date = row.getCell(10); //sandeep
			String val=null;
			String amt = null;

			if(tpsl_cell != null)
			{
				String response=null;
				String reference_id=null;
				String email_id=null;
				String required_loan_id=null; //sandeep
				String transaction_date = formatter.formatCellValue(trxn_date);; //sandeep
				val=formatter.formatCellValue(tpsl_cell);
				amt=formatter.formatCellValue(net_amount_cell);

				//getting email from excel file
				String email_and_phone=formatter.formatCellValue(email_cell);
				String[] result = email_and_phone.split(":");
				String[] store_email=result[1].split("}");

				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.xpath(" //*[@id=\"root\"]/div/div[2]/div/div[1]/div/div[1]/div[2]/div/div/a")).click();
				driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul/li[1]/a")).click();
				driver.findElement(By.xpath("/html/body/span/span/div/div/div[2]/div/div/div/ul/li/input")).sendKeys(val);
				driver.findElement(By.xpath(" //*[ contains (text(), 'Update filter' ) ]")).click();

								try {
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				reference_id=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div")).getText();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				email_id = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[4]/div")).getText();
				required_loan_id = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[5]/div")).getText(); //sandeep	
				System.out.println("############ "+ required_loan_id + " - "+ email_id +" - "+ reference_id);
				//					transaction_date = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[4]/div")).getText(); //sandeep
								} catch(Exception e) {}

				String countRows=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/span/a/span[1]")).getText();
				String[] event=countRows.split(" ");
				int count = Integer.parseInt(event[1]);

				//sandeep singh
				values[0] = required_loan_id;
				values[1] = amt; //sandeep
				values[2] = val;
				values[3] = "PAYNIMO";
				values[4] = store_email[0];
				values[5] = transaction_date;


				if( count == 1 ) {

					if(email_id.equals(store_email[0]) && reference_id.equals(val)) {

						String amount = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/div")).getText();
						double amount_in_metabase=Double.parseDouble(amount);
						double net_amount=Double.parseDouble(formatter.formatCellValue(net_amount_cell));

						if(net_amount == amount_in_metabase) {

							response = "Correct";

						} else {

							response = "Incorrect Net Amount";
						}

					} else {

						response = " Email OR Reference_id did not match";
					}


				} else {

					if(count == 0) {
						response = "Entry not created";	

					} else if(count > 1) {

						response = "Duplicate entry created";

					} else {
						response =" Something is worng";
					}
				}

				if(response != "Correct" && response != null)
				{
					test=true;
					values[6] = response;
					writer.writeNext(values);
					row.createCell((row.getLastCellNum())+1).setCellValue(response);
				} else {
					System.out.println("Entries are Correct");
					row.createCell(row.getLastCellNum()).setCellValue(response);
				}

			} 		
		} 
		writer.flush(); //sandeep

		FileOutputStream fos = new FileOutputStream(filePath);
		workbook.write(fos);
		workbook.close();	
		Assert.assertTrue(test,"Found some thing wrong");	
	}
}
